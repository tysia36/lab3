/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package permissions.domain;

/**
 *
 * @author j.pluzinska
 */
public class User {
	
	

	private int id;
	
    private String login;
    private String password;
    
    public User() {
	}
	
	public User(String login, String pass) {
		super();
		this.login = login;
		this.password = pass;
	}
    
    public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
    public String getLogin() 
    {
	return this.login;
    }

    public void setLogin(String login) 
    {
	this.login = login;
    }
    
    public String getPassword() 
    {
	return this.password;
    }

    public void setPassword(String password) 
    {
	this.password = password;
    }
}


