/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package permissions.domain;

/**
 *
 * @author j.pluzinska
 */
public class Permission {
    
	private int id;
    private String permission;
	
    public Permission() {
 	}
 	
 	public Permission(String permission) {
 		super();
 		this.permission=permission;
 	}
     
     public int getId() {
 		return id;
 	}

 	public void setId(int id) {
 		this.id = id;
 	}
	
	public String getPermission() {
		return this.permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}
        
}

