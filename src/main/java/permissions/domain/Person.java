/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package permissions.domain;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author j.pluzinska
 */
public class Person {
	
	private int id;
    private String name;
    private String surname;
    private List listA = new ArrayList();
    private List listTel = new ArrayList();
    private List listRole = new ArrayList();
    private User u = new User();
    
    public Person() {
   	}
   	
   	public Person(String name, String surname) {
   		super();
   		this.name = name;
   		this.surname = surname;
   	}
       
       public int getId() {
   		return id;
   	}

   	public void setId(int id) {
   		this.id = id;
   	}
    
    public User getU() {
		return u;
	}

	public void setU(User u) {
		this.u = u;
	}

	public List getListA() {
		return listA;
	}

	public List getListTel() {
		return listTel;
	}

	public List getListRole() {
		return listRole;
	}

	public String getName() 
    {
	return this.name;
    }

    public void setName(String name) 
    {
	this.name = name;
    }
    public String getSurname() 
    {
	return this.surname;
    }

    public void setSurname(String surname) 
    {
	this.surname = surname;
    }

	public void setListA(List listA) {
		this.listA = listA;
	}

	public void setListTel(List listTel) {
		this.listTel = listTel;
	}

	public void setListRole(List listRole) {
		this.listRole = listRole;
	}
        
    
}
