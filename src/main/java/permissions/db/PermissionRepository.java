package permissions.db;

import java.util.List;

import permissions.domain.Permission;

public interface PermissionRepository extends Repository<Permission> {
	
	public List<Permission> withPermission(String permission, PagingInfo page);
}