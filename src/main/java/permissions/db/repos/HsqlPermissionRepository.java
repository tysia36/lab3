package permissions.db.repos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import permissions.db.PagingInfo;
import permissions.db.PermissionRepository;
import permissions.domain.Permission;

public class HsqlPermissionRepository implements PermissionRepository{

	private Connection connection;
	
	private String insertSql ="INSERT INTO permission(permission) VALUES(?)"; 
	private String selectSql ="SELECT * FROM permission LIMIT(?)";
	private String selectByIdSql ="SELECT * FROM permission WHERE id=?";
	private String selectByPermissionSql ="SELECT * FROM permission WHERE Permission=? LIMIT(?)";
	private String deleteSql = "DELETE FROM permission WHERE id=?";
	private String updateSql = "UPDATE permission SET (permission)=(?) WHERE id=?";
	
	private PreparedStatement insert;
	private PreparedStatement select;
	private PreparedStatement selectById;
	private PreparedStatement selectByPermission;
	private PreparedStatement delete;
	private PreparedStatement update;
	

	private String createPermissionTable =""
			+ "CREATE TABLE Permission("
			+ "id bigint GENERATED BY DEFAULT AS IDENTITY,"
			+ "permission VARCHAR(20),"
			+ ")";
	
	
	public HsqlPermissionRepository(Connection connection){
		this.connection=connection;
		
		try{
			

			insert = connection.prepareStatement(insertSql);
			select = connection.prepareStatement(selectSql);
			selectById = connection.prepareStatement(selectByIdSql);
			selectByPermission = connection.prepareStatement(selectByPermissionSql);
			delete = connection.prepareStatement(deleteSql);
			update = connection.prepareStatement(updateSql);
			
			
			ResultSet rs = connection.getMetaData().getTables(null, null, null, null);
			
			boolean tableExists =false;
			while(rs.next())
			{
				if(rs.getString("TABLE_NAME").equalsIgnoreCase("Permission")){
					tableExists=true;
					break;
				}
			}
			if(!tableExists){
				Statement createTable = connection.createStatement();
				createTable.executeUpdate(createPermissionTable);
			}
		}catch(SQLException ex){
			ex.printStackTrace();
		}
		
	}
	
	
	public Permission withId(int id) {
		Permission result = null;
		try {
			selectById.setInt(1, id);
			ResultSet rs = selectById.executeQuery();
			while(rs.next()){
				Permission p = new Permission();
				p.setPermission(rs.getString("permission"));
				p.setId(rs.getInt("id"));
				result = p;
				break;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public List<Permission> allOnPage(PagingInfo page) {
List<Permission> result = new ArrayList<Permission>();
		
		try {
			select.setInt(1, page.getCurrentPage()*page.getSize());
			select.setInt(2, page.getSize());
			ResultSet rs = select.executeQuery();
			while(rs.next()){
				Permission p = new Permission();
				p.setPermission(rs.getString("permission"));
				p.setId(rs.getInt("id"));
				result.add(p);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public void add(Permission p) {
		try {
			insert.setString(1, p.getPermission());
			
			insert.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public void modify(Permission p) {
		try {
			update.setString(1, p.getPermission());
			update.setInt(2, p.getId());
			update.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public void remove(Permission p) {
		try {
			delete.setInt(1, p.getId());
			delete.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public List<Permission> withPermission(String permission, PagingInfo page) {
		
		List<Permission> result = new ArrayList<Permission>();
		try {
			selectByPermission.setString(1, permission);
			selectByPermission.setInt(2, page.getCurrentPage()*page.getSize());
			selectByPermission.setInt(3, page.getSize());
			ResultSet rs = selectByPermission.executeQuery();
			while(rs.next()){
				Permission p = new Permission();
				p.setPermission(rs.getString("permission"));
				p.setId(rs.getInt("id"));
				result.add(p);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

}
